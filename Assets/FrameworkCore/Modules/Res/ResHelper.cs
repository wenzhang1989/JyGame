﻿/*
 *  date: 2018-07-18
 *  author: John-chen
 *  cn: 资源加载流程
 *  en: todo:
 */

using UnityEngine;
using System.Collections;

namespace JyFramework
{
    /*
 * 资源加载流程：
 *  1.初始化的时候，读取file.txt文件并且整理<FileText>
 *  2.传入路径与<FileText>做对比，如果没有则直接加载Resources内的资源
 *  3.传入路径与<FileText>做对比，确定在StreamingAssets还是在persistentDataPath内，则相应加载对应文件夹内的资源
 *  4.加载的时候同步加载与异步加载接口都有提供，给使用者自由选择
 *  5.引用管理<>
 */

    /*
     * 不同平台下的加载
     * 
     * public void ResTest()
     * {
     *     switch(Application.platform)
     *     {
     *         case RuntimePlatform.Android: 
     *             break;
     *         case RuntimePlatform.IPhonePlayer:
     *             break;
     *         case RuntimePlatform.WindowsEditor:
     *             break;
     *     }            
     * }
     */

    /*
    * 加载资源的几种方式
    * 1.移动设备上加载资源 通过加载Application.persistentDataPath 内的资源
    * 2.Unity Editor上加载资源 
    *      1>Resource文件夹内加载资源
    *      2>StreamingAsset文件夹内加载资源
    *      
    * 统一的有两种加载资源
    *      1>同步加载
    *      2>异步加载
    */



    public class TestWWW : MonoBehaviour
    {
        // StreamingAssets 文件夹内的资源是只读的
        // 可以通过 <io> / <www> / <assetbundle> 几种模式加载资源
        // PersistentDataPath 文件夹内的资源是可读写的
        // 该文件夹内的资源只能通过 <io> / <www> 的方式加载

        /*
         * 对于www
         *      每次new WWW的时候，Unity都会开启一个线程去经行下载，通过此方式读取或者下载资源会在内存中
         *  生成WebStream，WebStream为下载文件转换后的内容，占用内存较大。使用WWW.Dispose()将终止仍在加载过程中的进程，
         *  并且会释放内存中的WebStream。
         *  如果WWW不及时释放，将会占用大量的内存空间，推荐搭配using方式使用
         */

        public IEnumerator TestDispose(string pathUrl)
        {
            WWW www = new WWW(pathUrl);
            try
            {
                yield return www;
                var txt = www.text;
                var texture = www.texture;
            }
            finally
            {
                www.Dispose();
            }
        }

        public IEnumerator TestUsing(string pathUrl)
        {
            using (WWW www = new WWW(pathUrl))
            {
                yield return www;
                var txt = www.text;
                var texture = www.texture;
            }
        }

        // WWW.LoadFromCacheOrDownload 方式
        /*
         *      如果使用次方式加载，将先从硬盘上的存储区查找是否有对应的资源，再验证本地Version与传入值之间的关系，
         *  如果传入的Version > 本地，则从传入的url地址下载资源，并缓存到硬盘替换掉现有资源，
         * 
         * 
         * 
         */

    }
}